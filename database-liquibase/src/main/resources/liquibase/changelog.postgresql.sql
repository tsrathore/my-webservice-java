--liquibase formatted sql

--changeset tsr:1
create table employee(
empid int primary key,
name varchar(255)
);

--rollback drop table employee;

